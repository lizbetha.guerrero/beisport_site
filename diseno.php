<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>: Beisport : </title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/font-awesome.min.css"> <!-- Font Awesome -->
  <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="css/wow-animate.css"> <!-- Wow Animate -->
  <link rel="stylesheet" href="css/style.css">
</head>
<script type="text/javascript" src="js/breinsport.js?v=202004081250"></script>
    <body onload="javascript:funcionDeAjuste();">
       
    <div class="header" id="top">
        <nav class="navbar navbar-inverse" role="navigation">
          <div class="container">
              <div class="navbar-header">
                  <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <a href="#" class="navbar-brand scroll-top"><img src="images/logo.png" alt="Layer Template"></a>
              </div>
              <!--/.navbar-header-->
              <div id="main-nav" class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                  <li><a href="index.html">Inicio</a></li>
                  <li><a href="#">Productos</a>
                    <ul class="sub-menu">
                      <li><a href="#">h1</a></li>
                      <li><a href="#">h2</a></li>
                      <li><a href="#">h3</a></li>
                    </ul>
                  </li>
                  <li><a href="diseno.html">Diseña tu uniforme</a></li>
                  
                </ul>
              </div>
              <!--/.navbar-collapse-->
      		</div>
      		<!--/.container-->
      </nav>
          <!--/.navbar-->
    </div>
    <!--/.header-->

      <div class="elements">
    <div class="container">
        <div class="row">
        <div class="elements-headings">
                <h2>Diseña tu uniforme</h2>
              </div>
          <div class="2-comulns-text">
            <div class="col-md-6">
              
            </div>
            <div class="col-md-12">
              <div class="col-md-6 ">
                <!-- Inicio - Imagen con el monito -->
                	
                <?php include 'customizer/imagenConMonito.php'; ?>
                <!-- fin - Imagen con el monito -->
              </div>  
              <div class="col-md-2"></div>
              <div class="col-md-4 back_opt">
              	<!-- Inicio - Arreglo con opciones -->
                <?php include 'customizer/arregloConOpciones.php'; ?>
                <!-- fin - Arreglo con opciones -->
              </div>          
            </div>
          </div>
        </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="2-comulns-text">
          <div class="col-md-6">
            
          </div>
          <div class="col-md-12">
            <!-- Inicio - Nuevo Formato
            ================================================== -->
            <div class="" style="margin:0 auto; width: 100%;">
			
            	<div class="col-md-12">
                    <form id="contactForm" method="post" action="envia.php" class="" role="form">
            			<div class="fom_tab">
							<div class="fom_tab_elem col-md-6">
                            		<label for="nombre" class="red1">Nombre</label>
                            	    <div class="">
                                        <input type="text" class="" id="nombre" name="nombre" placeholder="Su Nombre" required autocomplete="off" style="width: 100%; ">
                                    </div>
                              
                                    <label for="email" class="red1">Email</label>
                                    <div class="">
                                        <input type="email" class="" id="email" name="email" placeholder="Su E-mail" required autocomplete="off" style="width: 100%;">
                                    </div>
                                    <label for="teléfono" class="red1">Tel&eacute;fono</label>
                                    <div class="">
                                        <input type="tel" class="" id="telefono" name="telefono" placeholder="Su teléfono" autocomplete="off" style="width: 100%;">
                                    </div>
                                </div>
                                <div class="fom_tab_elem col-md-6">
                                    
                                    <label for="mensaje" class="red1">Mensaje</label>
                                    <div class="">
                                         <textarea class="" rows="4" id="mensaje" name="mensaje" placeholder="Su Mensaje..." required style="resize: none; width: 100%;"></textarea>
                                         
                                    </div>
                                    <div class="row esp">
                                    <div class="button mod_but_n"  id="enviar">Enviar</div>
                                    <div class="button mod_but_n1"  id="limpiaCampos">Limpiar</div>
</div>
                                  </div>
    						</div>
    				
              </form>
              </div>
                    <div class="col-md-10 col-md-offset-1 not">
                        <ul>
                            <li>Esta herramienta tiene unicamente la finalidad ayudarle a elaborar una configuraci&oacute;n aproximada del uniforme de beisbol solicitado. En ning&uacute;n momento significa que ser&aacute; la apariencia final del producto solicitado.</li>
                            <li>Los colores son aproximados a los reales e indistintamente pueden variar. En breve iremos a&ntilde;adiendo mas caracter&iacute;sticas a esta herramienta para mejorarla y que le sea de mayor utilidad.</li>
                            <li>Los datos que usted env&iacute;a solamente se utilizar&aacute;n para brindarle la informaci&oacute;n solicitada por usted y en ning&uacute;n momento se compartir&aacute;n con terceras personas o empresas.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Fin - Nuevo Formato
            ================================================== -->
          </div>
        </div>
      </div>
  </div>
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="social-icons">
            <ul>
              <li><a href="#"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="copyright-text">
          <p>Copyright &copy; 2020 Beisport</p>
        </div>
      </div>
      <div class="col-md-12">
        <div class="third-arrow">
          <a href="#" class="scroll-link btn btn-dark" data-id="top"><i class="fa fa-angle-up"></i></a>
        </div>
      </div>
    </div>
  </footer>
	</div>
    <script src="js/vendor/jquery-1.11.1.min.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
	<!-- templatemo 438 layer -->

  </body>
    <script type="text/javascript">
      function funcionPorcentaje(numero) {
        document.getElementById('muneco').style = "zoom: " + numero + "%; -moz-transform: scale(" + numero + "%); -webkit-transform: scale(" + numero + "%); top: 0; left: 0; text-align: left; vertical-align: top;";
      };
//991 767 640 480 320
      function funcionDeAjuste() {
        var ventana_ancho = $(window).width();
        if(ventana_ancho > 991) {
          this.funcionPorcentaje(100);
        } else if(ventana_ancho <= 991 && ventana_ancho > 767) {
          this.funcionPorcentaje(100);
        } else if(ventana_ancho <= 767 && ventana_ancho > 640) {
          this.funcionPorcentaje(90);
        } else if(ventana_ancho <= 640 && ventana_ancho > 480) {
          this.funcionPorcentaje(70);
        } else if(ventana_ancho <= 480 && ventana_ancho > 320) {
          this.funcionPorcentaje(60);
        } else if(ventana_ancho <= 320) {
          this.funcionPorcentaje(25);
        } else {
          this.funcionPorcentaje(100);
        }
      };
    
		document.getElementById('pa-0010').style = 'cursor: pointer; border: 1px none #000000; background-image: url(\'customizer/Texturas/Rayas.png\'); border-radius: 20px; height: 14px; width: 20px;';
		document.getElementById('ca-0013').style = 'cursor: pointer; border: 1px none #000000; background-image: url(\'customizer/Texturas/Rayas.png\'); border-radius: 20px; height: 14px; width: 20px;';
		document.getElementById('ca-0014').style = 'cursor: pointer; border: 1px none #000000; background-image: url(\'customizer/Texturas/Militar.png\'); border-radius: 20px; height: 14px; width: 20px;';
		document.getElementById('ma-0013').style = 'cursor: pointer; border: 1px none #000000; background-image: url(\'customizer/Texturas/Rayas.png\'); border-radius: 20px; height: 14px; width: 20px;';
		document.getElementById('ma-0014').style = 'cursor: pointer; border: 1px none #000000; background-image: url(\'customizer/Texturas/Militar.png\'); border-radius: 20px; height: 14px; width: 20px;';
		//document.getElementById('monitoDiv').style = "height: " + document.getElementById('mo-001').height + "px;" + "width: " + document.getElementById('mo-001').width + "px;";
	</script>
</html>