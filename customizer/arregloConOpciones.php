<?php ?>
<div style="display: inline-block; vertical-align: top;">
	<table style="border: 2px none; border-spacing: 0px; border-collapse: separate; width: 150px;">
		<tr>
			<td style="color: #000000; font-family: arial, helvetica, 'sans-serif'; font-size: 12px; height: 18px; text-align: left; vertical-align: middle; padding: 2px;">
				<font class="title_opt">Gorra</font>
			</td>
		</tr>
		<tr>
			<td style="height: 15px; text-align: left; vertical-align: top; padding: 2px;">			
				<input id="go-0001" value="" type="button" onclick="javascript:mostrarGorraByColor(0);" style="cursor: pointer; border: 1px solid #000000; background-color: #ffffff; border-radius: 10px; height: 14px; width: 20px;"/>
				<input id="go-0002" value="" type="button" onclick="javascript:mostrarGorraByColor(1);" style="cursor: pointer; border: 1px none #000000; background-color: #33006f; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="go-0003" value="" type="button" onclick="javascript:mostrarGorraByColor(2);" style="cursor: pointer; border: 1px none #000000; background-color: #c0c0c0; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="go-0004" value="" type="button" onclick="javascript:mostrarGorraByColor(3);" style="cursor: pointer; border: 1px none #000000; background-color: #272661; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="go-0005" value="" type="button" onclick="javascript:mostrarGorraByColor(4);" style="cursor: pointer; border: 1px none #000000; background-color: #000000; border-radius: 20px; height: 14px; width: 20px;"/>
			</td>
		</tr>
		<tr>
			<td style="height: 15px; text-align: left; vertical-align: top; padding: 2px;">
				<input id="go-0006" value="" type="button" onclick="javascript:mostrarGorraByColor(5);" style="cursor: pointer; border: 1px none #000000; background-color: #003831; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="go-0007" value="" type="button" onclick="javascript:mostrarGorraByColor(6);" style="cursor: pointer; border: 1px none #000000; background-color: #cda434; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="go-0008" value="" type="button" onclick="javascript:mostrarGorraByColor(7);" style="cursor: pointer; border: 1px none #000000; background-color: #c6011f; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="go-0009" value="" type="button" onclick="javascript:mostrarGorraByColor(8);" style="cursor: pointer; border: 1px none #000000; background-color: #800040; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="go-0010" value="" type="button" onclick="javascript:mostrarGorraByColor(9);" style="cursor: pointer; border: 1px none #000000; background-color: #df4601; border-radius: 20px; height: 14px; width: 20px;"/>
			</td>
		</tr>
		<tr>
			<td style="height: 15px; text-align: left; vertical-align: top; padding: 2px;">
				<input id="go-0011" value="" type="button" onclick="javascript:mostrarGorraByColor(10);" style="cursor: pointer; border: 1px none #000000; background-color: #ffcd00; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="go-0012" value="" type="button" onclick="javascript:mostrarGorraByColor(11);" style="cursor: pointer; border: 1px none #000000; background-color: #005a9c; border-radius: 20px; height: 14px; width: 20px;"/>
			</td>
		</tr>
		<tr>
			<td style="color: #000000; font-family: arial, helvetica, 'sans-serif'; font-size: 12px; height: 18px; text-align: left; vertical-align: middle; padding: 2px;">
				<font class="title_opt">Camisola</font>
			</td>
		</tr>
		<tr>
			<td style="height: 15px; text-align: left; vertical-align: top; padding: 2px;">			
				<input id="ca-0001" value="" type="button" onclick="javascript:mostrarCamisolaByColor(0);" style="cursor: pointer; border: 1px solid #000000; background-color: #ffffff; border-radius: 10px; height: 14px; width: 20px;"/>
				<input id="ca-0002" value="" type="button" onclick="javascript:mostrarCamisolaByColor(1);" style="cursor: pointer; border: 1px none #000000; background-color: #33006f; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="ca-0003" value="" type="button" onclick="javascript:mostrarCamisolaByColor(2);" style="cursor: pointer; border: 1px none #000000; background-color: #c0c0c0; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="ca-0004" value="" type="button" onclick="javascript:mostrarCamisolaByColor(3);" style="cursor: pointer; border: 1px none #000000; background-color: #272661; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="ca-0005" value="" type="button" onclick="javascript:mostrarCamisolaByColor(4);" style="cursor: pointer; border: 1px none #000000; background-color: #000000; border-radius: 20px; height: 14px; width: 20px;"/>
			</td>
		</tr>
		<tr>
			<td style="height: 15px; text-align: left; vertical-align: top; padding: 2px;">
				<input id="ca-0006" value="" type="button" onclick="javascript:mostrarCamisolaByColor(5);" style="cursor: pointer; border: 1px none #000000; background-color: #003831; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="ca-0007" value="" type="button" onclick="javascript:mostrarCamisolaByColor(6);" style="cursor: pointer; border: 1px none #000000; background-color: #cda434; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="ca-0008" value="" type="button" onclick="javascript:mostrarCamisolaByColor(7);" style="cursor: pointer; border: 1px none #000000; background-color: #c6011f; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="ca-0009" value="" type="button" onclick="javascript:mostrarCamisolaByColor(8);" style="cursor: pointer; border: 1px none #000000; background-color: #800040; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="ca-0010" value="" type="button" onclick="javascript:mostrarCamisolaByColor(9);" style="cursor: pointer; border: 1px none #000000; background-color: #df4601; border-radius: 20px; height: 14px; width: 20px;"/>
			</td>
		</tr>
		<tr>
			<td style="height: 15px; text-align: left; vertical-align: top; padding: 2px;">
				<input id="ca-0011" value="" type="button" onclick="javascript:mostrarCamisolaByColor(10);" style="cursor: pointer; border: 1px none #000000; background-color: #ffcd00; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="ca-0012" value="" type="button" onclick="javascript:mostrarCamisolaByColor(11);" style="cursor: pointer; border: 1px none #000000; background-color: #005a9c; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="ca-0013" value="" type="button" onclick="javascript:mostrarCamisolaByColor(12);" style=""/>
				<input id="ca-0014" value="" type="button" onclick="javascript:mostrarCamisolaByColor(13);" style=""/>
			</td>
		</tr>
		<tr>
			<td style="color: #000000; font-family: arial, helvetica, 'sans-serif'; font-size: 12px; height: 18px; text-align: left; vertical-align: middle; padding: 2px;">
				<font class="title_opt">Mangas</font>
			</td>
		</tr>
		<tr>
			<td style="height: 15px; text-align: left; vertical-align: top; padding: 2px;">			
				<input id="ma-0001" value="" type="button" onclick="javascript:mostrarMangasByColor(0);" style="cursor: pointer; border: 1px solid #000000; background-color: #ffffff; border-radius: 10px; height: 14px; width: 20px;"/>
				<input id="ma-0002" value="" type="button" onclick="javascript:mostrarMangasByColor(1);" style="cursor: pointer; border: 1px none #000000; background-color: #33006f; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="ma-0003" value="" type="button" onclick="javascript:mostrarMangasByColor(2);" style="cursor: pointer; border: 1px none #000000; background-color: #c0c0c0; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="ma-0004" value="" type="button" onclick="javascript:mostrarMangasByColor(3);" style="cursor: pointer; border: 1px none #000000; background-color: #272661; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="ma-0005" value="" type="button" onclick="javascript:mostrarMangasByColor(4);" style="cursor: pointer; border: 1px none #000000; background-color: #000000; border-radius: 20px; height: 14px; width: 20px;"/>
			</td>
		</tr>
		<tr>
			<td style="height: 15px; text-align: left; vertical-align: top; padding: 2px;">
				<input id="ma-0006" value="" type="button" onclick="javascript:mostrarMangasByColor(5);" style="cursor: pointer; border: 1px none #000000; background-color: #003831; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="ma-0007" value="" type="button" onclick="javascript:mostrarMangasByColor(6);" style="cursor: pointer; border: 1px none #000000; background-color: #cda434; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="ma-0008" value="" type="button" onclick="javascript:mostrarMangasByColor(7);" style="cursor: pointer; border: 1px none #000000; background-color: #c6011f; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="ma-0009" value="" type="button" onclick="javascript:mostrarMangasByColor(8);" style="cursor: pointer; border: 1px none #000000; background-color: #800040; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="ma-0010" value="" type="button" onclick="javascript:mostrarMangasByColor(9);" style="cursor: pointer; border: 1px none #000000; background-color: #df4601; border-radius: 20px; height: 14px; width: 20px;"/>
			</td>
		</tr>
		<tr>
			<td style="height: 15px; text-align: left; vertical-align: top; padding: 2px;">
				<input id="ma-0011" value="" type="button" onclick="javascript:mostrarMangasByColor(10);" style="cursor: pointer; border: 1px none #000000; background-color: #ffcd00; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="ma-0012" value="" type="button" onclick="javascript:mostrarMangasByColor(11);" style="cursor: pointer; border: 1px none #000000; background-color: #005a9c; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="ma-0013" value="" type="button" onclick="javascript:mostrarMangasByColor(12);" style=""/>
				<input id="ma-0014" value="" type="button" onclick="javascript:mostrarMangasByColor(13);" style=""/>
			</td>
		</tr>
		<tr>
			<td style="color: #000000; font-family: arial, helvetica, 'sans-serif'; font-size: 12px; height: 18px; text-align: left; vertical-align: middle; padding: 2px;">
				<font class="title_opt">Sudadera</font>
			</td>
		</tr>
		<tr>
			<td style="height: 15px; text-align: left; vertical-align: top; padding: 2px;">			
				<input id="su-0001" value="" type="button" onclick="javascript:mostrarSudaderaByColor(0);" style="cursor: pointer; border: 1px solid #000000; background-color: #ffffff; border-radius: 10px; height: 14px; width: 20px;"/>
				<input id="su-0002" value="" type="button" onclick="javascript:mostrarSudaderaByColor(1);" style="cursor: pointer; border: 1px none #000000; background-color: #33006f; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="su-0003" value="" type="button" onclick="javascript:mostrarSudaderaByColor(2);" style="cursor: pointer; border: 1px none #000000; background-color: #c0c0c0; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="su-0004" value="" type="button" onclick="javascript:mostrarSudaderaByColor(3);" style="cursor: pointer; border: 1px none #000000; background-color: #272661; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="su-0005" value="" type="button" onclick="javascript:mostrarSudaderaByColor(4);" style="cursor: pointer; border: 1px none #000000; background-color: #000000; border-radius: 20px; height: 14px; width: 20px;"/>
			</td>
		</tr>
		<tr>
			<td style="height: 15px; text-align: left; vertical-align: top; padding: 2px;">
				<input id="su-0006" value="" type="button" onclick="javascript:mostrarSudaderaByColor(5);" style="cursor: pointer; border: 1px none #000000; background-color: #003831; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="su-0007" value="" type="button" onclick="javascript:mostrarSudaderaByColor(6);" style="cursor: pointer; border: 1px none #000000; background-color: #cda434; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="su-0008" value="" type="button" onclick="javascript:mostrarSudaderaByColor(7);" style="cursor: pointer; border: 1px none #000000; background-color: #c6011f; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="su-0009" value="" type="button" onclick="javascript:mostrarSudaderaByColor(8);" style="cursor: pointer; border: 1px none #000000; background-color: #800040; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="su-0010" value="" type="button" onclick="javascript:mostrarSudaderaByColor(9);" style="cursor: pointer; border: 1px none #000000; background-color: #df4601; border-radius: 20px; height: 14px; width: 20px;"/>
			</td>
		</tr>
		<tr>
			<td style="height: 15px; text-align: left; vertical-align: top; padding: 2px;">
				<input id="su-0011" value="" type="button" onclick="javascript:mostrarSudaderaByColor(10);" style="cursor: pointer; border: 1px none #000000; background-color: #ffcd00; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="su-0012" value="" type="button" onclick="javascript:mostrarSudaderaByColor(11);" style="cursor: pointer; border: 1px none #000000; background-color: #005a9c; border-radius: 20px; height: 14px; width: 20px;"/>
			</td>
		</tr>
		<tr>
			<td style="color: #000000; font-family: arial, helvetica, 'sans-serif'; font-size: 12px; height: 18px; text-align: left; vertical-align: middle; padding: 2px;">
				<font class="title_opt">Pantal&oacute;n</font>
			</td>
		</tr>
		<tr>
			<td style="height: 15px; text-align: left; vertical-align: top; padding: 2px;">			
				<input id="pa-0001" value="" type="button" onclick="javascript:mostrarPantalonByColor(0);" style="cursor: pointer; border: 1px solid #000000; background-color: #ffffff; border-radius: 10px; height: 14px; width: 20px;"/>
				<input id="pa-0002" value="" type="button" onclick="javascript:mostrarPantalonByColor(1);" style="cursor: pointer; border: 1px none #000000; background-color: #33006f; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="pa-0003" value="" type="button" onclick="javascript:mostrarPantalonByColor(2);" style="cursor: pointer; border: 1px none #000000; background-color: #c0c0c0; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="pa-0004" value="" type="button" onclick="javascript:mostrarPantalonByColor(3);" style="cursor: pointer; border: 1px none #000000; background-color: #272661; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="pa-0005" value="" type="button" onclick="javascript:mostrarPantalonByColor(4);" style="cursor: pointer; border: 1px none #000000; background-color: #000000; border-radius: 20px; height: 14px; width: 20px;"/>
			</td>
		</tr>
		<tr>
			<td style="height: 15px; text-align: left; vertical-align: top; padding: 2px;">
				<input id="pa-0006" value="" type="button" onclick="javascript:mostrarPantalonByColor(5);" style="cursor: pointer; border: 1px none #000000; background-color: #003831; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="pa-0007" value="" type="button" onclick="javascript:mostrarPantalonByColor(6);" style="cursor: pointer; border: 1px none #000000; background-color: #cda434; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="pa-0008" value="" type="button" onclick="javascript:mostrarPantalonByColor(7);" style="cursor: pointer; border: 1px none #000000; background-color: #c6011f; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="pa-0009" value="" type="button" onclick="javascript:mostrarPantalonByColor(8);" style="cursor: pointer; border: 1px none #000000; background-color: #800040; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="pa-0010" value="" type="button" onclick="javascript:mostrarPantalonByColor(9);" style=""/>
			</td>
		</tr>
		<tr>
			<td style="color: #000000; font-family: arial, helvetica, 'sans-serif'; font-size: 12px; height: 18px; text-align: left; vertical-align: middle; padding: 2px;">
				<font class="title_opt">Medias</font>
			</td>
		</tr>
		<tr>
			<td style="height: 15px; text-align: left; vertical-align: top; padding: 2px;">			
				<input id="me-0002" value="" type="button" onclick="javascript:mostrarMediasByColor(1);" style="cursor: pointer; border: 1px none #000000; background-color: #33006f; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="me-0004" value="" type="button" onclick="javascript:mostrarMediasByColor(3);" style="cursor: pointer; border: 1px none #000000; background-color: #272661; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="me-0005" value="" type="button" onclick="javascript:mostrarMediasByColor(4);" style="cursor: pointer; border: 1px none #000000; background-color: #000000; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="me-0006" value="" type="button" onclick="javascript:mostrarMediasByColor(5);" style="cursor: pointer; border: 1px none #000000; background-color: #003831; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="me-0007" value="" type="button" onclick="javascript:mostrarMediasByColor(6);" style="cursor: pointer; border: 1px none #000000; background-color: #cda434; border-radius: 20px; height: 14px; width: 20px;"/>
			</td>
		</tr>
		<tr>
			<td style="height: 15px; text-align: left; vertical-align: top; padding: 2px;">
				<input id="me-0008" value="" type="button" onclick="javascript:mostrarMediasByColor(7);" style="cursor: pointer; border: 1px none #000000; background-color: #c6011f; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="me-0009" value="" type="button" onclick="javascript:mostrarMediasByColor(8);" style="cursor: pointer; border: 1px none #000000; background-color: #800040; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="me-0010" value="" type="button" onclick="javascript:mostrarMediasByColor(9);" style="cursor: pointer; border: 1px none #000000; background-color: #df4601; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="me-0011" value="" type="button" onclick="javascript:mostrarMediasByColor(10);" style="cursor: pointer; border: 1px none #000000; background-color: #ffcd00; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="me-0012" value="" type="button" onclick="javascript:mostrarMediasByColor(11);" style="cursor: pointer; border: 1px none #000000; background-color: #005a9c; border-radius: 20px; height: 14px; width: 20px;"/>
			</td>
		</tr>
	</table>
</div>
<div style="display: inline-block; vertical-align: top;">
	<table style="border: 2px none; border-spacing: 0px; border-collapse: separate; width: 150px;">
		<tr>
			<td style="color: #000000; font-family: arial, helvetica, 'sans-serif'; font-size: 12px; height: 18px; text-align: left; vertical-align: middle; padding: 2px;">
				<font class="title_opt">Visera</font>
			</td>
		</tr>
		<tr>
			<td style="height: 15px; text-align: left; vertical-align: top; padding: 2px;">			
				<input id="vi-0001" value="" type="button" onclick="javascript:mostrarViseraByColor(0);" style="cursor: pointer; border: 1px solid #000000; background-color: #ffffff; border-radius: 10px; height: 14px; width: 20px;"/>
				<input id="vi-0002" value="" type="button" onclick="javascript:mostrarViseraByColor(1);" style="cursor: pointer; border: 1px none #000000; background-color: #33006f; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="vi-0003" value="" type="button" onclick="javascript:mostrarViseraByColor(2);" style="cursor: pointer; border: 1px none #000000; background-color: #c0c0c0; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="vi-0004" value="" type="button" onclick="javascript:mostrarViseraByColor(3);" style="cursor: pointer; border: 1px none #000000; background-color: #272661; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="vi-0005" value="" type="button" onclick="javascript:mostrarViseraByColor(4);" style="cursor: pointer; border: 1px none #000000; background-color: #000000; border-radius: 20px; height: 14px; width: 20px;"/>
			</td>
		</tr>
		<tr>
			<td style="height: 15px; text-align: left; vertical-align: top; padding: 2px;">
				<input id="vi-0006" value="" type="button" onclick="javascript:mostrarViseraByColor(5);" style="cursor: pointer; border: 1px none #000000; background-color: #003831; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="vi-0007" value="" type="button" onclick="javascript:mostrarViseraByColor(6);" style="cursor: pointer; border: 1px none #000000; background-color: #cda434; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="vi-0008" value="" type="button" onclick="javascript:mostrarViseraByColor(7);" style="cursor: pointer; border: 1px none #000000; background-color: #c6011f; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="vi-0009" value="" type="button" onclick="javascript:mostrarViseraByColor(8);" style="cursor: pointer; border: 1px none #000000; background-color: #800040; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="vi-0010" value="" type="button" onclick="javascript:mostrarViseraByColor(9);" style="cursor: pointer; border: 1px none #000000; background-color: #df4601; border-radius: 20px; height: 14px; width: 20px;"/>
			</td>
		</tr>
		<tr>
			<td style="height: 15px; text-align: left; vertical-align: top; padding: 2px;">
				<input id="vi-0011" value="" type="button" onclick="javascript:mostrarViseraByColor(10);" style="cursor: pointer; border: 1px none #000000; background-color: #ffcd00; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="vi-0012" value="" type="button" onclick="javascript:mostrarViseraByColor(11);" style="cursor: pointer; border: 1px none #000000; background-color: #005a9c; border-radius: 20px; height: 14px; width: 20px;"/>
			</td>
		</tr>
		<tr>
			<td style="color: #000000; font-family: arial, helvetica, 'sans-serif'; font-size: 12px; height: 18px; text-align: left; vertical-align: middle; padding: 2px;">
				<font class="title_opt">Vivos Camisola</font>
			</td>
		</tr>
		<tr>
			<td style="height: 15px; text-align: left; vertical-align: top; padding: 2px;">			
				<input id="vica-0001" value="" type="button" onclick="javascript:mostrarVivosCamisolaByColor(0);" style="cursor: pointer; border: 1px solid #000000; background-color: #ffffff; border-radius: 10px; height: 14px; width: 20px;"/>
				<input id="vica-0002" value="" type="button" onclick="javascript:mostrarVivosCamisolaByColor(1);" style="cursor: pointer; border: 1px none #000000; background-color: #33006f; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="vica-0003" value="" type="button" onclick="javascript:mostrarVivosCamisolaByColor(2);" style="cursor: pointer; border: 1px none #000000; background-color: #c0c0c0; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="vica-0004" value="" type="button" onclick="javascript:mostrarVivosCamisolaByColor(3);" style="cursor: pointer; border: 1px none #000000; background-color: #272661; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="vica-0005" value="" type="button" onclick="javascript:mostrarVivosCamisolaByColor(4);" style="cursor: pointer; border: 1px none #000000; background-color: #000000; border-radius: 20px; height: 14px; width: 20px;"/>
			</td>
		</tr>
		<tr>
			<td style="height: 15px; text-align: left; vertical-align: top; padding: 2px;">
				<input id="vica-0006" value="" type="button" onclick="javascript:mostrarVivosCamisolaByColor(5);" style="cursor: pointer; border: 1px none #000000; background-color: #003831; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="vica-0007" value="" type="button" onclick="javascript:mostrarVivosCamisolaByColor(6);" style="cursor: pointer; border: 1px none #000000; background-color: #cda434; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="vica-0008" value="" type="button" onclick="javascript:mostrarVivosCamisolaByColor(7);" style="cursor: pointer; border: 1px none #000000; background-color: #c6011f; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="vica-0009" value="" type="button" onclick="javascript:mostrarVivosCamisolaByColor(8);" style="cursor: pointer; border: 1px none #000000; background-color: #800040; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="vica-0010" value="" type="button" onclick="javascript:mostrarVivosCamisolaByColor(9);" style="cursor: pointer; border: 1px none #000000; background-color: #df4601; border-radius: 20px; height: 14px; width: 20px;"/>
			</td>
		</tr>
		<tr>
			<td style="height: 15px; text-align: left; vertical-align: top; padding: 2px;">
				<input id="vica-0011" value="" type="button" onclick="javascript:mostrarVivosCamisolaByColor(10);" style="cursor: pointer; border: 1px none #000000; background-color: #ffcd00; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="vica-0012" value="" type="button" onclick="javascript:mostrarVivosCamisolaByColor(11);" style="cursor: pointer; border: 1px none #000000; background-color: #005a9c; border-radius: 20px; height: 14px; width: 20px;"/>
			</td>
		</tr>
		<tr>
			<td style="color: #000000; font-family: arial, helvetica, 'sans-serif'; font-size: 12px; height: 18px; text-align: left; vertical-align: middle; padding: 2px;">
				<font class="title_opt">Vivos Mangas</font>
			</td>
		</tr>
		<tr>
			<td style="height: 15px; text-align: left; vertical-align: top; padding: 2px;">			
				<input id="vima-0001" value="" type="button" onclick="javascript:mostrarVivosMangasByColor(0);" style="cursor: pointer; border: 1px solid #000000; background-color: #ffffff; border-radius: 10px; height: 14px; width: 20px;"/>
				<input id="vima-0002" value="" type="button" onclick="javascript:mostrarVivosMangasByColor(1);" style="cursor: pointer; border: 1px none #000000; background-color: #33006f; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="vima-0003" value="" type="button" onclick="javascript:mostrarVivosMangasByColor(2);" style="cursor: pointer; border: 1px none #000000; background-color: #c0c0c0; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="vima-0004" value="" type="button" onclick="javascript:mostrarVivosMangasByColor(3);" style="cursor: pointer; border: 1px none #000000; background-color: #272661; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="vima-0005" value="" type="button" onclick="javascript:mostrarVivosMangasByColor(4);" style="cursor: pointer; border: 1px none #000000; background-color: #000000; border-radius: 20px; height: 14px; width: 20px;"/>
			</td>
		</tr>
		<tr>
			<td style="height: 15px; text-align: left; vertical-align: top; padding: 2px;">
				<input id="vima-0006" value="" type="button" onclick="javascript:mostrarVivosMangasByColor(5);" style="cursor: pointer; border: 1px none #000000; background-color: #003831; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="vima-0007" value="" type="button" onclick="javascript:mostrarVivosMangasByColor(6);" style="cursor: pointer; border: 1px none #000000; background-color: #cda434; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="vima-0008" value="" type="button" onclick="javascript:mostrarVivosMangasByColor(7);" style="cursor: pointer; border: 1px none #000000; background-color: #c6011f; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="vima-0009" value="" type="button" onclick="javascript:mostrarVivosMangasByColor(8);" style="cursor: pointer; border: 1px none #000000; background-color: #800040; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="vima-0010" value="" type="button" onclick="javascript:mostrarVivosMangasByColor(9);" style="cursor: pointer; border: 1px none #000000; background-color: #df4601; border-radius: 20px; height: 14px; width: 20px;"/>
			</td>
		</tr>
		<tr>
			<td style="height: 15px; text-align: left; vertical-align: top; padding: 2px;">
				<input id="vima-0011" value="" type="button" onclick="javascript:mostrarVivosMangasByColor(10);" style="cursor: pointer; border: 1px none #000000; background-color: #ffcd00; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="vima-0012" value="" type="button" onclick="javascript:mostrarVivosMangasByColor(11);" style="cursor: pointer; border: 1px none #000000; background-color: #005a9c; border-radius: 20px; height: 14px; width: 20px;"/>
			</td>
		</tr>
		<tr>
			<td style="color: #000000; font-family: arial, helvetica, 'sans-serif'; font-size: 12px; height: 18px; text-align: left; vertical-align: middle; padding: 2px;">
				<font class="title_opt">Cinturon</font>
			</td>
		</tr>
		<tr>
			<td style="height: 15px; text-align: left; vertical-align: top; padding: 2px;">			
				<input id="ci-0002" value="" type="button" onclick="javascript:mostrarCinturonByColor(1);" style="cursor: pointer; border: 1px none #000000; background-color: #33006f; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="ci-0004" value="" type="button" onclick="javascript:mostrarCinturonByColor(3);" style="cursor: pointer; border: 1px none #000000; background-color: #272661; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="ci-0005" value="" type="button" onclick="javascript:mostrarCinturonByColor(4);" style="cursor: pointer; border: 1px none #000000; background-color: #000000; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="ci-0006" value="" type="button" onclick="javascript:mostrarCinturonByColor(5);" style="cursor: pointer; border: 1px none #000000; background-color: #003831; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="ci-0007" value="" type="button" onclick="javascript:mostrarCinturonByColor(6);" style="cursor: pointer; border: 1px none #000000; background-color: #cda434; border-radius: 20px; height: 14px; width: 20px;"/>
			</td>
		</tr>
		<tr>
			<td style="height: 15px; text-align: left; vertical-align: top; padding: 2px;">
				<input id="ci-0008" value="" type="button" onclick="javascript:mostrarCinturonByColor(7);" style="cursor: pointer; border: 1px none #000000; background-color: #c6011f; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="ci-0009" value="" type="button" onclick="javascript:mostrarCinturonByColor(8);" style="cursor: pointer; border: 1px none #000000; background-color: #800040; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="ci-0010" value="" type="button" onclick="javascript:mostrarCinturonByColor(9);" style="cursor: pointer; border: 1px none #000000; background-color: #df4601; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="ci-0012" value="" type="button" onclick="javascript:mostrarCinturonByColor(11);" style="cursor: pointer; border: 1px none #000000; background-color: #005a9c; border-radius: 20px; height: 14px; width: 20px;"/>
			</td>
		</tr>
		<tr>
			<td>
				&nbsp;
			</td>
		</tr>
		<tr>
			<td style="color: #000000; font-family: arial, helvetica, 'sans-serif'; font-size: 12px; height: 18px; text-align: left; vertical-align: middle; padding: 2px;">
				<font class="title_opt">Vivos Pantal&oacute;n</font>
			</td>
		</tr>
		<tr>
			<td style="height: 15px; text-align: left; vertical-align: top; padding: 2px;">			
				<input id="vipa-0001" value="" type="button" onclick="javascript:mostrarVivosPantalonByColor('pr-002', '27Blanco');" style="cursor: pointer; border: 1px solid #000000; background-color: #ffffff; border-radius: 10px; height: 14px; width: 20px;"/>
				<input id="vipa-0002" value="" type="button" onclick="javascript:mostrarVivosPantalonByColor('pr-002', '26Purpura');" style="cursor: pointer; border: 1px none #000000; background-color: #33006f; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="vipa-0003" value="" type="button" onclick="javascript:mostrarVivosPantalonByColor('pr-002', '28GrisPlata');" style="cursor: pointer; border: 1px none #000000; background-color: #c0c0c0; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="vipa-0004" value="" type="button" onclick="javascript:mostrarVivosPantalonByColor('pr-002', '25Azul');" style="cursor: pointer; border: 1px none #000000; background-color: #272661; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="vipa-0005" value="" type="button" onclick="javascript:mostrarVivosPantalonByColor('pr-002', '29Negro');" style="cursor: pointer; border: 1px none #000000; background-color: #000000; border-radius: 20px; height: 14px; width: 20px;"/>
			</td>
		</tr>
		<tr>
			<td style="height: 15px; text-align: left; vertical-align: top; padding: 2px;">
				<input id="vipa-0006" value="" type="button" onclick="javascript:mostrarVivosPantalonByColor('pr-002', '23Verde');" style="cursor: pointer; border: 1px none #000000; background-color: #003831; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="vipa-0007" value="" type="button" onclick="javascript:mostrarVivosPantalonByColor('pr-002', '21Oro');" style="cursor: pointer; border: 1px none #000000; background-color: #cda434; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="vipa-0008" value="" type="button" onclick="javascript:mostrarVivosPantalonByColor('pr-002', '19Rojo');" style="cursor: pointer; border: 1px none #000000; background-color: #c6011f; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="vipa-0009" value="" type="button" onclick="javascript:mostrarVivosPantalonByColor('pr-002', '18Guinda');" style="cursor: pointer; border: 1px none #000000; background-color: #800040; border-radius: 20px; height: 14px; width: 20px;"/>
				<input id="vipa-0010" value="" type="button" onclick="javascript:mostrarVivosPantalonByColor('pr-002', '20Naranja');" style="cursor: pointer; border: 1px none #000000; background-color: #df4601; border-radius: 20px; height: 14px; width: 20px;"/>
			</td>
		</tr>
		<tr>
			<td style="height: 15px; text-align: left; vertical-align: top; padding: 2px;">
				<input id="vipa-0011" value="" type="button" onclick="javascript:mostrarVivosPantalonByColor('pr-002', '24AzulMedio');" style="cursor: pointer; border: 1px none #000000; background-color: #005a9c; border-radius: 20px; height: 14px; width: 20px;"/>
			</td>
		</tr>
	</table>
</div>